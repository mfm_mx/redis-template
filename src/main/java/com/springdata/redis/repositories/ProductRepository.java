package com.springdata.redis.repositories;

import org.springframework.data.repository.CrudRepository;

import com.springdata.redis.model.Product;

public interface ProductRepository extends CrudRepository<Product, String> {

}

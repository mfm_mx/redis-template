package com.springdata.redis.services;

import java.util.List;

import com.springdata.redis.model.Product;

public interface ProductService {
	List<Product> listAll();

	Product getById(String id);

	Product saveOrUpdate(Product product);

	void delete(String id);
}

package com.springdata.redis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springdata.redis.model.Product;
import com.springdata.redis.services.ProductService;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

	@Autowired
	ProductService productService;

	@PostMapping("/getById/{id}")
	public ResponseEntity<Product> getByIdPathVariable(@PathVariable(required = true) String id) {

		Product product = productService.getById(id);

		if (product == null) {
			return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@PostMapping("/getById")
	public ResponseEntity<Product> getByIdRequestParam(@RequestParam(required = true) String id) {
		Product product = productService.getById(id);

		if (product == null) {
			return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public Product saveOrUpdateProduct(@RequestBody Product product) {
		Product savedProduct = productService.saveOrUpdate(product);
		return savedProduct;
	}

	@RequestMapping(value = "/product/v2", method = RequestMethod.POST)
	public ResponseEntity<Product> saveOrUpdateProductV2(@RequestBody Product product) {
		Product savedProduct = productService.saveOrUpdate(product);
		return new ResponseEntity<Product>(savedProduct, HttpStatus.OK);
	}

	@PostMapping("/removeById/{id}")
	public Boolean removeById(@PathVariable(required = true) String id) {

		Product product = productService.getById(id);

		if (product == null) {
			return false;
		}
		productService.delete(id);
		return true;
	}

}
